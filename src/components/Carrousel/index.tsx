import { FiArrowLeft, FiArrowRight } from 'react-icons/fi'
import styles from './styles.module.scss'

type Movies = {
  poster_path: string
  original_title: string
}

type CarrouselProps = {
  title: string
  allMoives: Movies[]
}

export default function Carrousel({ title, allMoives }: CarrouselProps) {
  return (
    <div className={styles.movieRow}>
      <h2>{title}</h2>
      <div className={styles.movieRowLeft}>
        <FiArrowLeft />
      </div>

      <div className={styles.movieRowRight}>
        <FiArrowRight />
      </div>

      <div className={styles.movieRowListarea}>
        <div className={styles.movieRowList}>
          {allMoives.map((items, key) => {
            return (
              <div key={key} className={styles.movieRowItem}>
                <img
                  src={`https://image.tmdb.org/t/p/w300${items.poster_path}`}
                  alt={items.original_title}
                />
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}
