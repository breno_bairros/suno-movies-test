import { MdSend, MdClose } from 'react-icons/md'
import { motion } from 'framer-motion'
import styles from './styles.module.scss'

type SearchProps = {
  eventSubmit: () => void
  eventCloseSearch: () => void
  changeValue: () => void
}

export default function Search({
  eventSubmit,
  eventCloseSearch,
  changeValue,
  ...rest
}: SearchProps) {
  return (
    <motion.div
      exit={{ opacity: 1 }}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
    >
      <div className={styles.container}>
        <div className={styles.speed}>
          <form onSubmit={eventSubmit}>
            <div className={styles.inputBlock}>
              <input type="text" onChange={changeValue} {...rest} />
              <MdSend color="#f73ec2" size={24} />
            </div>
          </form>
          <button type="button" onClick={eventCloseSearch}>
            <MdClose color="#f73ec2" size={24} />
          </button>
        </div>
      </div>
    </motion.div>
  )
}
