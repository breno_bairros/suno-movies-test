import { useState } from 'react'
import { motion } from 'framer-motion'
import { MdSearch, MdMenu } from 'react-icons/md'
import styles from './header.module.scss'

type HeaderProps = {
  eventOpenSearch: () => void
}

export default function Header({ eventOpenSearch }: HeaderProps) {
  const [isMobile, setIsMobile] = useState(false)

  const eventeOpenMenuMobile = () => {
    setIsMobile(true)
  }

  return (
    <>
      <header className={styles.container}>
        <div className={styles.menuMobile}>
          <button type="button" onClick={eventeOpenMenuMobile}>
            <MdMenu size={24} />
          </button>
        </div>
        <section className={styles.title}>
          <h4>Suno</h4>
          <h4 className={styles.titleSuno}>Movies</h4>
        </section>

        <section className={styles.groups}>
          <a href="#">Início</a>
          <a href="#">Catálogo</a>
          <button type="button" onClick={eventOpenSearch}>
            <MdSearch size={24} />
          </button>
        </section>
      </header>

      <motion.div
        exit={{ opacity: 0 }}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
      >
        <section
          className={
            isMobile ? styles.mobileLinksActive : styles.mobileLinksDisable
          }
        >
          <a href="#">Início</a>
          <a href="#">Catálogo</a>
        </section>
      </motion.div>
    </>
  )
}
