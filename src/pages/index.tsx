import { useState } from 'react'
import Header from '../components/Header'
import Search from '../components/Search'

export default function Home() {
  const [isSearch, setIsSearch] = useState(false)

  const eventForOpenSearch = () => {
    setIsSearch(true)
  }

  const eventForCloseSearch = () => {
    setIsSearch(false)
  }

  return (
    <>
      <Header eventOpenSearch={eventForOpenSearch} />
      {isSearch && (
        <Search
          eventSubmit={() => {}}
          eventCloseSearch={eventForCloseSearch}
          changeValue={() => {}}
        />
      )}
    </>
  )
}
